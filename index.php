<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 1:19 AM
 */

     ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
<!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">
        .error{
    padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
         .navbar-brand {
    color: #26617d;
    margin-left: 23%;
             margin-bottom: 2%;

        }

          .navbar-brand:hover{
    color: #4c99ab;
}

        .navbar-brand img{
    display: inline-block;
}
        hr{
    border-color: #4e9aac;
        }
        a:hover{
    text-decoration: none;
        }
    </style>
</head>
<body>






<div class="container">
    <h1 class="welcome text-center"> </h1>
    <div class="card card-container">

        <!--<h2 class='login_title text-center'>Login</h2>-->
        <a href="/" class="navbar-brand">


        </a>
        <hr>
        <p id="message" class="text-center alert  alert-danger error" hidden> </p>

        <form class="form-signin" id="fetchdetails" method="post" action="details.php">
            <span id="reauth-email" class="reauth-email"></span>
            <p class="input_title">Enter Url</p>
            <input type="text" id="url" name="url" class="login_box" placeholder="http://www.commerce.com" required autofocus>



            <button class="btn btn-lg btn-primary" id="check" type="button">Login</button>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->

<script type="text/javascript" src="/js/jquery.min.js"></script>

<script type="text/javascript">

    $(function () {

        /**
         * input validation
         */

        $("#check").on("click",function () {

            var url = $("#url").val();
            var is_valid ;

            var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;

            is_valid = pattern.test(url);

             var error = false;


            if(url == null || url == "" || url== undefined){

                Error("message","url  is empty !",3000);

                $("#url").focus();
                error = true;

                return false;

            }

            if(!is_valid){
                Error("message","Enter a valid url!",3000);

                $("#url").focus();
                error = true;

                return false;
            }






            if(!error){

                $("#fetchdetails").submit();
//                    e.preventDefault();





            }


        });
    });






    /**
     * function for error show
     */

    function Error(id,message,time) {

        $("#"+id).text(message).show();

        setTimeout(function () {
            $("#"+id).hide();
        },time);
    }





</script>

</body>
</html>