  <?php
  /**
   * Created by PhpStorm.
   * User: Sandeep Maurya
   * Date: 11/14/2017
   * Time: 1:32 AM
   */

  ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">

        html,body{
            background: radial-gradient(ellipse at center, rgba(185,210,224,1) 0%,rgba(187,214,228,1) 0%,rgba(186,211,225,1) 15%,rgba(186,211,225,1) 38%,rgba(169,199,215,1) 68%,rgba(169,199,215,1) 68%,rgba(169,199,215,1) 82%,rgba(158,191,208,1) 100%);
        }
        .error{
            padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
        .navbar-brand {
            color: #26617d;
            margin-left: 23%;
            margin-bottom: 2%;

        }

        .navbar-brand:hover{
            color: #4c99ab;
        }

        .navbar-brand img{
            display: inline-block;
        }
        hr{
            border-color: #4e9aac;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body  >

<div class="container">
    <div class=" " style="width: 350px;margin:0 auto 25px;background: #afd9ee;">
        <div class="  text-center" style="background: #fcfcfc;padding: 10px;margin-top: 40px;" >


            <?php


            if($_SERVER["REQUEST_METHOD"]=="POST") : ?>

                <?php

                $url = clean($_POST['url']);



                ?>
                <p><b>Load Time :</b> <?php echo http_loadtime($url) . "  seconds";?></p>


                <?php


                $htmlcode = http_header($url);
                echo "<br/> ";
                echo "<b>HTTP Code: </b> $htmlcode<br>";
                ?>


                <?php


                $wrapper = fopen('php://temp', 'r+'); // Opening a wrapper in php
                $ch = curl_init($url);         // Initilizing Curl
                curl_setopt($ch, CURLOPT_VERBOSE, true);
                curl_setopt($ch, CURLOPT_STDERR, $wrapper);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); // Executing Curl & storing Data in result
                curl_close($ch); // Closing Curl
                $ips = get_curl_remote_ips($wrapper); // Getting remote IPS from the wrapper into the IPS Variable
                fclose($wrapper); // Closing Wrapper
                echo "<b>IP Address: </b>";
                echo end($ips)."<br>";  // Printing last IP address

                ?>





                <?php


                $html = file_get_contents_curl($url); // Calling the function
// Starting of Parsing
                $doc = new DOMDocument(); //creating New Dom Document
                @$doc->loadHTML($html); // Loading HTML Source in docs
                $nodes = $doc->getElementsByTagName('title'); // getting the attributes of Title Tags
//Putting Details into a variable
                $title = $nodes->item(0)->nodeValue; // Getting title
                $metas = $doc->getElementsByTagName('meta'); //getting Elements with the tags meta
// Seperating meta description and meta keyword  with each other with running a loop.
            $keywords ="";
                $description ="";
                for ($i = 0; $i < $metas->length; $i++)
                {
                    $meta = $metas->item($i);
                    if($meta->getAttribute('name') == 'description') //Getting attributes with the Attribute "description"
                        $description = $meta->getAttribute('content'); // Loading Content of meta description into a variable
                    if($meta->getAttribute('name') == 'keywords') //Getting attributes with the Attribute "keywords"
                        $keywords = $meta->getAttribute('content'); // Loading keyword content into a variable
                }
                echo "<br><b>Title:</b> $title". '<br/><br/>'; // Printing The Title
                echo "<b>Description:</b> $description". '<br/><br/>'; // Printing The Description
// Checking if any keyword is returned or not if not Print NA
                if ($keywords == "")
                {
                    echo "<b>Keywords :</b> NA" . '<br/><br/>';
                }
                else
                {
                    echo "<b>Keywords: </b> $keywords" . '<br/><br/>';
                }

                ?>



                <?php

                echo "<br/>";
                echo "<b>Internal and External Link lists : </b>";
                echo "<br/> ";

                $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
// make the cURL request to $target_url
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_AUTOREFERER, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                $html= curl_exec($ch);
                if (!$html) {
                    echo "<br />cURL error number:" .curl_errno($ch);
                    echo "<br />cURL error:" . curl_error($ch);
                    exit;
                }
// parse the html into a DOMDocument
                $dom = new DOMDocument();
                @$dom->loadHTML($html);
//grab titles and all the things
// grab all the on the page
                $xpath = new DOMXPath($dom);
                $hrefs = $xpath->evaluate("/html/body//a");
                for ($i = 0; $i < $hrefs->length; $i++) {
                    $href = $hrefs->item($i);
                    $url1 = $href->getAttribute('href');
                    checkLink($url1,$url);
                }

                ?>



            <?php else : ?>

                <?php echo "Some Errors Occurred | please try later"; ?>
                <?php header("location: index.php") ;?>
            <?php endif ;?>

        </div>
    </div>
</div>




</body>
</html>

   <?php
  function clean($data){

    $data = trim($data);

    $data = stripslashes($data);

    $data = htmlspecialchars($data);

    return $data;

}




function http_loadtime($url) {
    $handle = curl_init($url);
    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
    // gtting the Page Contents into Response
    $response = curl_exec($handle);
    // Calculating Loadtime Using Curl Inbuilt Function
    $loadtime = curl_getinfo($handle, CURLINFO_TOTAL_TIME);
    curl_close($handle);
    return $loadtime;
}



   function checkLink($url,$gathered_from) {
       if(filter_var($url, FILTER_VALIDATE_URL)){
           $position = strpos($url, $gathered_from);
           if($position !== FALSE)
           {
               echo "<b>Internal Link: </b> ";
               echo "$url";
               echo "<br/>";
           }
           else
           {
               echo "<b>External Link : </b> ";
               echo "$url";
               echo "<br/>";
           }
       }
   }


   function http_header($url) {
       $handle = curl_init($url);
       curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
       // Get the HTML Code in the response
       $response = curl_exec($handle);
       // Checking http code through curl Inbuilt Function
       $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
       curl_close($handle);

       return $httpCode;
   }



   function get_curl_remote_ips($fp)
   {
       rewind($fp); // Getting IP Adderss into fp variable
       $str = fread($fp, 8192); // Reading IP address
       $regex = '/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/'; // Regular Expression for the Valid IP adress matching
       if (preg_match_all($regex, $str, $matches)) {
           return array_unique($matches[0]);
       } else {
           return false;
       }
   }


   /**
    * keywords meta,
    */
   function file_get_contents_curl($url)
   {
       $ch = curl_init(); //Initializing Curl
       curl_setopt($ch, CURLOPT_HEADER, 0);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
       $data = curl_exec($ch); //Executing CURL Function for getting the data
       curl_close($ch); // Closing Curl
       return $data; // Returning the HTML Page through data
   }



?>